package dk.xakeps.jfx.truevfs.patcher.deployment;

import io.quarkus.test.QuarkusUnitTest;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class TruevfsPatcherTest {
    @RegisterExtension
    static final QuarkusUnitTest config = new QuarkusUnitTest()
//            .overrideConfigKey("quarkus.class-loading.parent-first-artifacts",
//                    "net.java.truevfs:truevfs-driver-tar-gzip,net.java.truevfs:truevfs-access,net.java.truevfs:truevfs-kernel-impl,net.java.truevfs:truevfs-driver-file,net.java.truevfs:truevfs-comp-tardriver,net.java.truevfs:truevfs-kernel-spec")
            .setArchiveProducer(() -> ShrinkWrap.create(JavaArchive.class)
                    .addAsResource("test.tar.gz"));

    @Disabled
    @Test
    public void test() throws URISyntaxException, IOException {
        URL resourceUrl = getClass().getResource("/test.tar.gz");
        Assertions.assertNotNull(resourceUrl);
        URI testResourceUri = resourceUrl.toURI();
        try (FileSystem fileSystem = FileSystems.newFileSystem(Path.of(testResourceUri), Thread.currentThread().getContextClassLoader())) {
            Path rootPath = fileSystem.getPath("/");
            Files.walk(rootPath).forEach(System.out::println);
        }
    }
}
