// Copyright 2021 Xakep_SDK
// ASM patcher for TrueVFS

package dk.xakeps.jfx.truevfs.patcher.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.BytecodeTransformerBuildItem;
import io.quarkus.deployment.builditem.FeatureBuildItem;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class TruevfsPatcherProcessor {

    private static final String FEATURE = "truevfs-patcher";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }

    @BuildStep
    BytecodeTransformerBuildItem transformTrueVfs() {
        return new BytecodeTransformerBuildItem.Builder()
                .setEager(true)
                .setClassToTransform("net.java.truevfs.access.TFileSystem$FsNodeAttributes")
                .setVisitorFunction((s, classVisitor) -> new ClassVisitorImpl(Opcodes.ASM9, classVisitor))
                .build();
    }

    private static class ClassVisitorImpl extends ClassVisitor {

        public ClassVisitorImpl(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("fileKey".equals(name) && "()Ljava/lang/Object;".equals(descriptor)) {
                return new FileKeyMethodVisitor(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }
            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private static class FileKeyMethodVisitor extends MethodVisitor {
        public FileKeyMethodVisitor(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
            visitInsn(Opcodes.ACONST_NULL);
            visitInsn(Opcodes.ARETURN);
        }
    }
}
